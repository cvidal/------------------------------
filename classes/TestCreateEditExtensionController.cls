@isTest
private class TestAutocompleteExtensionController {
	
	@isTest static void test_method_one() {
		Account acc = new Account();
		acc.Name = 'test';
		insert acc;

		Test.setCurrentPageReference(Page.ACAccount);
		ApexPages.currentPage().getParameters().put('id', acc.Id);

		ApexPages.standardController con = new ApexPages.standardController(acc);

		system.debug('myPage: ' + ApexPages.currentPage().getUrl());
		CreateEditExtensionController constructor = new CreateEditExtensionController(con);

		constructor.getSalutation();
	}
	
}